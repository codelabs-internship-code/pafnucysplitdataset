# PafnucySplitDataset #

This repo can be used to take the extracted contents of the PDBbind database and split it into the training, validation and core datasets for the Pafnucy neural network.

# Usage #

## Setting up Files ##

Open up `split_dataset.py`. First, set the versions of the PDBbind datasbe you are using.

```python
PDB_VERSION = '2019'
PDB_CORE_VERSION = '2016'
```

If the version of the PDBbind database your using does have a core set, (e.g. the 2013 or 2016 versions) then set `PDB_CORE_VERSION` to `PDB_VERSION`. If it doesn't, set `PDB_CORE_VERSION` to whichever version of the database you are using that does have a core set. For example as shown above, the 2019 version will be used for the training and validation sets, whereas the 2016 version will be used for the test set.

Next, we need to copy over the extracted contents of the PDBbind database to `./data`. Specifically,

* `index` - this directory can be found in `PDBbind_YYYY_plain_text_index` (where `YYYY` is the year) and should contain the index files for each set in the database.
* `general-set-except-refined` - this should contain all the general complexes, it should NOT have the refined complexes.
* `refined-set` - this contains all the refined complexes and (if applicable) the core complexes.
* `refined-set-YYYY` - you only need this if you are using the core set from a different version of the PDBbind database. `YYYY` is the year (e.g. 2016). If this is not the case, you don't need this directory.

The sturcture of your directories should look like this

```
PafnucySplitDataset
   -data
       -index
       -general-except-refined
       -refined-set
       -refined-set-YYYY
   -datasets
       -test
       -training
       -validation
    ...
```

`./datasets` is where the files for each dataset is stored. If you don't have these directories already, make sure you set them up as shown above before proceeding.

---

## Preparing Datasets ##

Preparing the datasets includes

* removing any entries in the PDBbind records we don't have files (`mol2`/`sdf`) for
* seperating the general, refined and core complexes
* gathering files for the validation set by randomly choosing 1000 refined complexes
* gathering files for the training set (which is composed the the remaining refined complexes and the general complexes)
* gathering the files for the test set from the core set of PDBbind
* storing the affinities for each complex for each dataset in a CSV

Once the files of PDBbind have been stored in `./data`, we can do everything listed above by simply running `python split_datasets.py`.

---

## Creating HDF's ##

To create the HDF for the training set, run the following:

```bash
python prepare.py -l datasets/training/*/*_ligand.mol2 -p datasets/training/*/*_pocket.pdb --pocket_format pdb --affinities datasets/training/affinities.csv -o datasets/training_set.hdf
```

To create the HDF for the validation set, run the following:

```bash
python prepare.py -l datasets/validation/*/*_ligand.mol2 -p datasets/validation/*/*_pocket.pdb --pocket_format pdb --affinities datasets/validation/affinities.csv -o datasets/validation_set.hdf
```

To create the HDF for the test set, run the following:

```bash
python prepare.py -l datasets/test/*/*_ligand.mol2 -p datasets/test/*/*_pocket.pdb --pocket_format pdb --affinities datasets/test/affinities.csv -o datasets/test_set.hdf
```

You can find the HDF's in `./datasets`.