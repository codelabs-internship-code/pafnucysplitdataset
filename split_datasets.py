from shutil import copytree, rmtree
from os import listdir, makedirs
from random import sample
import pandas as pd

# The version of pdbbind we will draw complexes from for the training and validation sets
PDB_VERSION = '2019'
# The version of pdbbind we will draw complexes from for the test set
PDB_CORE_VERSION = '2016'

# Paths to extracted pdbbind files
PATH_TO_RECORDS = './data/index/'
PATH_TO_GENERAL = './data/general-set-except-refined/'
PATH_TO_REFINED = './data/refined-set/'
PATH_TO_CORE = './data/refined-set-2016/'

# Paths to datasets
PATH_TO_TRAINING = './datasets/training/'
PATH_TO_VALIDATION = './datasets/validation/'
PATH_TO_TEST = './datasets/test/'

# Do some cleanup of the directories first
rmtree(PATH_TO_TRAINING)
makedirs(PATH_TO_TRAINING)
rmtree(PATH_TO_VALIDATION)
makedirs(PATH_TO_VALIDATION)
rmtree(PATH_TO_TEST)
makedirs(PATH_TO_TEST)

# Load the records
header = ['res', 'release_year', '-logKd/Ki', 'Kd/Ki/IC50', 'comment', 'ref', 'ligand_name']

# this still has the refined complexes - we will clean this up later
general = pd.read_csv('{0}INDEX_general_PL_data.{1}'.format(PATH_TO_RECORDS, PDB_VERSION), delim_whitespace=True, comment='#', names=header, index_col=0)
# this still has the core complexes - we will clean this up later
refined = pd.read_csv('{0}INDEX_refined_data.{1}'.format(PATH_TO_RECORDS, PDB_VERSION), delim_whitespace=True, comment='#', names=header, index_col=0)
core = pd.read_csv('{0}INDEX_core_data.{1}'.format(PATH_TO_RECORDS, PDB_CORE_VERSION), delim_whitespace=True, comment='#', names=header, index_col=0)

# Remove any entries in the general set that we don't have the files for
general_temp = general.copy(deep=True)

# basically a list of the general complexes we do have files on
valid_general_complexes = listdir(PATH_TO_GENERAL)

for cmplx in general.index:
    if not cmplx in valid_general_complexes:
        # print('dropping {}'.format(cmplx))
        general_temp.drop(cmplx, inplace=True)

general = general_temp

# Remove any entries in the refined set that we don't have the files for
# print('\n')
refined_temp = refined.copy(deep=True)
core_temp = core.copy(deep=True)

# basically a list of the refined complexes we do have files on
valid_refined_complexes = listdir(PATH_TO_REFINED)

for cmplx in refined.index:
    if not cmplx in valid_refined_complexes:
        # print('dropping {}'.format(cmplx))
        try:
            refined_temp.drop(cmplx, inplace=True)
        except:
            # remember, the refined set also contains the core complexes
            # print('warning: dropping from core set {0}'.format(cmplx))
            core_temp.drop(cmplx, inplace=True)

refined = refined_temp
core = core_temp

# clean up the general set so there are no refined complexes
general_except_refined = general.copy(deep=True)

refined_complexes = refined.index
core_complexes = core.index

for cmplx in general.index:
    if cmplx in refined_complexes:
        general_except_refined.drop(cmplx, inplace=True)

general_except_refined_complexes = general_except_refined.index

# clean up the refined set so there are no core complexes
refined_except_core = refined.copy(deep=True)

for cmplx in refined_complexes:
    if cmplx in core_complexes:
        refined_except_core.drop(cmplx, inplace=True)

refined_except_core_complexes = refined_except_core.index

# now all our datasets are ready:
# general_except_refined
# refined_except_core
# core

# so now we split the datasets, starting with validation set
validation_set = pd.DataFrame()

# generate 1000 random numbers to choose the complexes for the validation set
validation_complexes_indices = sample(range(0, len(refined_except_core_complexes)), 1000)

for index in validation_complexes_indices:
    pdbid = refined_except_core_complexes[index]
    entry = refined_except_core.loc[pdbid]
    validation_set = validation_set.append(entry)
    # drop this from the refined_except_core set for later on when making the training set
    refined_except_core.drop(pdbid, inplace=True)

# save the .csv with the binding affinities
data = {
    'name': validation_set.index,
    'affinity': validation_set['-logKd/Ki'].to_list(),
    'set': ['refined'] * 1000
}
validation_set_affinities = pd.DataFrame(data=data)

pdbids = validation_set_affinities['name']
for index in range(len(pdbids)):
    pdbids[index] = '{0}_ligand'.format(pdbids[index])

validation_set_affinities.to_csv('{0}affinities.csv'.format(PATH_TO_VALIDATION), index=False)

# copy over all the complexes in the validation set to a single folder
for cmplx in validation_set.index:
    src = '{0}{1}'.format(PATH_TO_REFINED, cmplx)
    dst = '{0}{1}'.format(PATH_TO_VALIDATION, cmplx)
    copytree(src, dst)

# now we can make the training set
# this is for the complexes that come from the general set
for cmplx in general_except_refined_complexes:
    src = '{0}{1}'.format(PATH_TO_GENERAL, cmplx)
    dst = '{0}{1}'.format(PATH_TO_TRAINING, cmplx)
    copytree(src, dst)

general_data = {
    'name': general_except_refined.index,
    'affinity': general_except_refined['-logKd/Ki'].to_list(),
    'set': ['general'] * len(general_except_refined_complexes)
}
general_data_affinities = pd.DataFrame(data=general_data)

pdbids = general_data_affinities['name']
for index in range(len(pdbids)):
    pdbids[index] = '{0}_ligand'.format(pdbids[index])

# this is for the complexes that come from the refined set
for cmplx in refined_except_core.index:
    src = '{0}{1}'.format(PATH_TO_REFINED, cmplx)
    dst = '{0}{1}'.format(PATH_TO_TRAINING, cmplx)
    copytree(src, dst)

refined_data = {
    'name': refined_except_core.index,
    'affinity': refined_except_core['-logKd/Ki'].to_list(),
    'set': ['refined'] * len(refined_except_core.index)
}
refined_data_affinities = pd.DataFrame(data=refined_data)

pdbids = refined_data_affinities['name']
for index in range(len(pdbids)):
    pdbids[index] = '{0}_ligand'.format(pdbids[index])

# finally, merge the two DataFrames and save the affinities
training_set_affinities = general_data_affinities.append(refined_data_affinities)
training_set_affinities.to_csv('{0}affinities.csv'.format(PATH_TO_TRAINING), index=False)

# lastly, the core set
for cmplx in core_complexes:
    src = '{0}{1}'.format(PATH_TO_CORE, cmplx)
    dst = '{0}{1}'.format(PATH_TO_TEST, cmplx)
    copytree(src, dst)

data = {
    'name': core_complexes,
    'affinity': core['-logKd/Ki'].to_list(),
    'set': ['core'] * len(core_complexes)
}
core_set_affinities = pd.DataFrame(data=data)

pdbids = core_set_affinities['name']
for index in range(len(pdbids)):
    pdbids[index] = '{0}_ligand'.format(pdbids[index])

core_set_affinities.to_csv('{0}affinities.csv'.format(PATH_TO_TEST), index=False)